## Davi Farhi

Game Engine Programmer at Asobo Studio<br>
Working on Microsoft Flight Simulator

Living in Bordeaux, Nouvelle-Aquitaine, France  
- [Cursus 42](https://gitlab.com/my42lausanne)
- [dotfile repo](https://gitlab.com/davifarhi/dotfiles)
- [Computer Vision Line Follower Robot](https://gitlab.com/genesis-robotics/opencv-rescue-line/jice19-alpha-centauri)
- [Guiding Light - Swiss Game Academy 2022](https://gitlab.com/davifarhi/sga2022)
- [Scop rendering engine](https://gitlab.com/my42lausanne/scop)